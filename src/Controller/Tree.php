<?php

namespace Drupal\composer_dependency_tree\Controller;

use Drupal\Core\Controller\ControllerBase;
use markfullmer\DependencyTree;

/**
 * The txg_engagement controller.
 */
class Tree extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function content() {
    $composer_path = dirname(DRUPAL_ROOT) . '/composer.json';
    if (file_exists($composer_path)) {
      $json_root = file_get_contents($composer_path);
    }
    $lock_path = dirname(DRUPAL_ROOT) . '/composer.lock';
    if (file_exists($lock_path)) {
      $json_lock = file_get_contents($lock_path);
    }
    $data = DependencyTree::generateTree($json_root, $json_lock, TRUE);
    $build = [
      '#markup' => '<figure id="tree"></figure>',
    ];
    $build['#attached']['library'][] = 'composer_dependency_tree/tree';
    $build['#attached']['drupalSettings']['composer_dependency_tree']['tree'] = $data;
    return $build;
  }

}
