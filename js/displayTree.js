(function ($, Drupal, drupalSettings) {
  function init(data) {
    if (typeof initialized == 'undefined') {
      var initialized = true;
      dependencyTree(data, 'figure#tree');
    }
  }
  Drupal.behaviors.composerDependencyTree = {
    attach: function (context, drupalSettings) {
      $(document, context).once('composerDependencyTree').each(function () {
        init(JSON.parse(drupalSettings.composer_dependency_tree.tree));
      });
    }
  };
})(jQuery, Drupal, drupalSettings);